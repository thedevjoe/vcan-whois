const BASE_WHOIS = "whois.iana.org";
const app = (require('express'))();
const net = require('net');
const axios = require('axios').default;
const fs = require('fs');
const bodyparser = require('body-parser');
const dns = require('dns');
const isIp = require('is-ip');
const {exec} = require('child_process');

app.use(bodyparser.json());

const VCAN_CONF = {
    name: 'whois',
    dashUri: 'http://localhost:8367/dash',
    webhooks: [
        {
            id: 'whois',
            label: 'Execute WHOIS query',
            type: 'client',
            url: 'http://localhost:8367/execute'
        }
    ],
    serviceId: 'whois'
};

axios.post('http://localhost:22646/register', VCAN_CONF).then(e => {
    console.log(e);
});

function ExecuteWhois(address, arin_only = false, hops = [], server = BASE_WHOIS, iana = true) {
    return new Promise((res, rej) => {
        console.log("Sending whois for " + address + " to " + server);
        let sock = new net.Socket();
        sock.setTimeout(1000);
        sock.setEncoding('utf-8');
        sock.on('connect', () => {
            console.log("CONNECT " + server);
            sock.write(address + "\r\n");
        });
        let d = "";
        sock.on('data', (data) => {
            //console.log("DATA", data);
            d += data;
        });
        sock.on('timeout', () => {
            hops.push({server, response: "Timed out waiting for response."});
            res(hops);
            sock.destroy();
        })
        sock.on('error', (e) => {
            hops.push({server, error: e});
            res(hops);
        });
        sock.on('close', (error) => {
            if(error) return;
            hops.push({server, response: d});
            console.log("Rcv " +d.length + " byte response");
            let ref = null;
            d.split('\n').forEach(l => {
                if(l.startsWith('%')) return;
                if(iana){
                    if(l.startsWith('refer:')) {
                        ref = l.substr(6).trim();
                    }
                }
                else {
                    if(l.trim().startsWith('Registrar WHOIS Server:')) {
                        ref = l.trim().substr(23).trim();
                    } else if(l.trim().startsWith('ReferralServer:')) {
                        ref = l.trim().substr(15).trim();
                    }
                }
            });
            if(ref != null) ref = ref.replace('whois://', '');
            if(ref != null && ref !== server && !arin_only) {
                return ExecuteWhois(address, false, hops, ref, false).then(res);
            }
            res(hops);
        })
        sock.connect(43, server);
    });
}

function ExecuteDig(address) {
    return new Promise((res, rej) => {
        if(isIp(address)) return res("DIG UNAVAILABLE FOR " + address);
        exec((process.env.DIG_LOCATION == null ? 'dig' : process.env.DIG_LOCATION) + " " + address, {}, (e, out, err) => {
            //console.log(e, out, err);
            res(out);
        });
    });
}

app.get('/dash', (req, res) => {
    res.send(fs.readFileSync('dash.html'));
});

app.post('/execute', (req, res) => {
    if(req.body.address === undefined) return res.sendStatus(400);
    const match = /^[a-z0-9._-]*$/;
    let addr = req.body.address;
    if(!match.test(addr)) return res.sendStatus(400);
    if(addr.split('.').length == 1) return res.sendStatus(400);
    let arinonly = (addr.split('.').length > 2) && !isIp(addr);
    let remarks = "";
    if(arinonly) {
        remarks += "Only ARIN WHOIS results available for this query.\n";
    }
    ExecuteWhois(req.body.address, arinonly).then(e => {
        //console.log(e);
        ExecuteDig(addr).then(dig => {
            let d = null;
            if(!isIp(req.body.address)) {
                dns.lookup(req.body.address, {family: 4}, (ee, a, f) => {
                    if(ee) {
                        return res.json({dns: null, hops: e, remarks, dig});
                    } else {
                        return res.json({dns: a, hops: e, remarks, dig});
                    }
                });
            } else
                res.json({dns: null, hops: e, remarks, dig});
        });
    });
});

app.listen(8367);